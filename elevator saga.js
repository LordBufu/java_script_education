/* Code Planning and documentation.
Things we need:
	- A array to store all elevator data. // done
	- A array to store all request data. // done
	- A function to build and update the elevator data. // done / cleanedup / read for testing.
	- A function to build and update the request data. // ontwerpen

	// maybe convert this add and/or remove to a update ?
	- A function add request data:
	- A remove function to remove request data:

Elevator list in a array as objects, array structure:
	- elNr	(vallue)	= elevator number.
	- state	(string)	= elevator state idle\moving.
	- cFloor(vallue)	= current floor.
	- cDir	(string)	= current elevator direction.
	- cLoad (?float)	= current elevator load.		// Apears broken atm, cant get any other vallue to return then 0 from the API, removed from code untill fixed was a minor change anyway.

Elevator requests in a array as objects, array structure:
	- elNr	(vallue)	= elevator number.
	- dir	(string)	= elevator direction.
	- flNr	(vallue)	= requested floor number.

TODO: Check if all cLoad entries are now gone, since the load function is not working properly atm it has to be removed entirely.
TODO: Clear/fix/solve all TODO entries in the code after finishing the main functions.
TODO: After testing each function clean up the comments to reduce overall code size and readability.
*/

// Data storage variables.
var elevatorList = [];
var elevatorRequests = [];

// Function to build and update the elevatorlist.
function updateElevatorList() {
	var listCache = elevatorList; // Copy current list into this cache variable.
	var updaterCache = []; // Empty cache variable we use to check elements against the current list.
	var updated = false; // Set to false by default for loop trigger.
	
	// First we need something to update the info of each elevator, and push that a full cachedlist we can work with.
	// TODO: Keep checking the API function to see if we get a return vallue from elevator load, to use an a full indicator.
	function updateCache() {
		// Use API object array to go true all elevators, and store the info we want untill complete and then push it into the main updaterCache.
		elevators.forEach(function(object, index) {
			elevatorCache = getData();
			
			// Function to get the data for the elevatorCache.
			function getData() {
				var state = getState();
				var cDir = getDirection();
				
				// Use API listen event(s) to check if elevator is idle or not, and return a bolean.
				function getState() {
					object.on('idle', function() { return true; });
					// TODO: find a more robust way to deal with this, once variable is set to true nothing will set to it to false currently.
					if(state === undefined) {
						return false;
					}
				};
				
				// Use API listen event to get the elevator its travel direction.
				// We could directly set cDirection (in the case of this training\excercise), but good practice would be to not set variables but let the code define them instead.
				function getDirection() {
					var cDirection;
					
					if(cDirection === undefined) { cDirection = 'none'; }
					
					object.on("passing_floor", function(floorNum, direction) { cDirection = direction; });
					
					return cDirection;
				};
				
				// Return data in array object format.
				return { elNr: index, state: state, cFloor: object.currentFloor(), cDir: cDir};
			};
			
			// Push elevatorCache into global updateCache for further use.
			updaterCache.push(elevatorCache);
		});
		
		// Before we exit we set updated to true, since all info is now up-to-date.
		updated = true;
	};
	
	// Now we check if updated = false (this is why we set it to false by default), 
	if(!updated) {
		// we run the function to update the cache first,
		updateCache();
		
		// then we check if the current copy of the list (listCache) has entries,
		if(listCache.length !== 0) {
			// we loop true the updateCache,
			for(i = 0; i < updaterCache.length; i++) {
				// we pass each element into our duplication checker,
				var duplicate = dupChecker(updaterCache[i].elNr, updaterCache[i].state, updaterCache[i].cFloor, updaterCache[i].cDir);
				
				function dupChecker(elNr, state, cFloor, cDir) {
					// then we go true each entry on the current list
					for(j = 0; j < listCache.lenght; j++) {
						// if we find a match in elevator Number,
						if(listCache[j].elNr == elNr) {
							// we check the other elements at update them where needed.
							if(listCache[j].state !== state) { listCache[j].state = state; }
							if(listCache[j].cFloor !== cFloor) { listCache[j].cFloor = cFloor; }
							if(listCache[j].cDir !== cDir) { listCache[j].cDir = cDir; }
						}
					}
				};
			}
		}
		// If there are no entries we push the current Cache into the list to populate it.
		else {
			elevatorList = updaterCache;
		}
	}
};

// Concept code, attempt to make a update/build function for request data.
//	updateRequestList function:
//		- Takes 5 inputs:
//			- elNr: elevator that needs to handle the request.
//			- flNr: the floor that was requested or requested from.
//			- dir: the direction of the request.
//			- add: bolean vallue to check if we need to add or remove.
//			- index: vallue used to remove requested entry.
//		- All inputs should pass a 'undefined' check:
//			- this check should be the main logic that decides what function to call (for example: if(!add) { removeRequest(index) } )
//		- removeRequest function:
//			- Takes 1 input:
//				- x = array index number.
//			- Splices the array data element, splice appends the index to match the new structure.
//		- addRequest function:
//			- Takes 3 inputs passed in via de main function:
//				- elNr, flNr, dir.
//			- Creates a variable checker function:
//				- If the elevatorRequests array has a length:
//					- We loop true all entries in the array:
//						- We check if elNr or flNr from the array against the elNr and flNr that is requested are equal:
//							- If one of the above is equal, we check if flNr and dir from both are equal:
//								- If equal we return true to the variable.
//							- If not nothing happens.
//						- If not nothing happens.
//				- If the elevatorRequests array has no length:			
//					- We return false to the variable.
//
// updateRequestList function:
//		- Has 5 inputs (2 optional):
//			- elNr: elevator that needs to handle the request.
//			- flNr: the that was requested or requested from.
//			- dir: the direction requested (only for floor requests).
//			- index: index of a entry.
//			- add: if something should be added true/false.
//		- Creates a cache for the current elevatorRequests.
//		- Caches the index and add data, that was either passed or not passed in.
//		- removeRequest function:
//			- Takes 1 input:
//				- x = index.
//			- Removes entry using splice, leaving no gap in the array.
function updateRequestList(elNr, flNr, dir, index, add) {
	var requestListCache = elevatorRequests;
	var indexCache = index;
	var addCache = add;

	// Simple and quick remove and append array index
	function removeRequest(x) { requestListCache.splice(x, 1); };
	
	// addRequest function structure:
	//		- Takes 3 statements (elNr, flNr, dir).
	//		- Creates a bolean variable for indicating a duplicate entry:
	//			- Checks the input against the current array entries, and returns a bolean based on its findings.
	//		- Creates a variable to store the request that was passed to the function:
	//			- Creates an object array out of the request, that can be pushed into the array.
	// 		- If the duplicate variable is not true, we push the info into the array if not true function will end.
	function addRequest(elNr, flNr, dir) {
		var duplicate = checker();
		var requestMemory =  request(elNr, flNr, dir);
		
		// zoek naar dubble entries
		function checker() {
			if(elevatorRequests.length !== 0) {
				for(i = 0; i < elevatorRequests.length; i++) {
					if(elevatorRequests[i].elNr == elNr || elevatorRequests[i].flNr == flNr) {
						if(elevatorRequests[i].flNr == flNr && elevatorRequests[i].dir == dir) {
							return true;
						}
					}
				}
			}
			else { return false; }
		};
		
		// return het verzoek als object
		function request(elNr, flNr, dir) { return { elNr: elNr, flNr: flNr, dir: dir }; }
		
		// als er geen dubble entries zijn, voegen wij het verzoek toe
		if(!duplicate) { elevatorRequests.push(requestMemory); }
	};
	
	// als add false is, moet de entry eruit
	if(!add) { removeRequest(index); }
	// als add niet false is, is hij true en moet er toegevoegt worden
	else { addRequest(elNr, flNr, dir); }
};

// Start logic //
// Just fooling around a litle bit //
if(elevatorList.length == 0) {
	updateElevatorList();
}
else {
	// For every floor in floors,
	floors.forEach(function(floor) {
		var random = pickRandom();	// pick random number for elevator since they all just press a button for any elevator.
		var upRequestCache = constrUpRequest();	// create up requests.
		var downRequestCache = constrDownRequest(); // create down requests.
		
		function pickRandom() {
			Math.floor(Math.random()*elevators.length);
		};
		
		function constrRequest() {
			floor.on('up_button_pressed', function () {
				var l_dir = 'up';
				return { elNr: random, flNr: floor.level, dir: l_dir, add: true };
			});
		};
		
		function constrDownRequest() {
			floor.on('down_button_pressed', function () {
				var l_dir = 'down';
				return { elNr: random, flNr: floor.level, dir: l_dir, add: true };
			});
		};
	});
}
